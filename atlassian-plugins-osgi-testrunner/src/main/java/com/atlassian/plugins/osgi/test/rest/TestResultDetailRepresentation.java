package com.atlassian.plugins.osgi.test.rest;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

/**
 * @since version
 */
public class TestResultDetailRepresentation extends TestDetailRepresentation
{

    @JsonProperty private Result testResult;
    @JsonProperty private Set<String> passedMethods;
    @JsonProperty private Set<String> ignoredMethods;
    @JsonProperty private Map<String,Failure> failedMethods;
    

    @JsonCreator
    public TestResultDetailRepresentation(@JsonProperty("classname") String classname, @JsonProperty("testResult") Result testResult)
    {
        super(classname);
        
        this.testResult = testResult;

        this.passedMethods = new HashSet<String>();
        this.ignoredMethods = new HashSet<String>();
        this.failedMethods = new HashMap<String, Failure>();
    }

    public Result getTestResult()
    {
        return testResult;
    }

    public Set<String> getPassedMethods()
    {
        return passedMethods;
    }

    public Set<String> getIgnoredMethods()
    {
        return ignoredMethods;
    }

    public Map<String,Failure> getFailedMethods()
    {
        return failedMethods;
    }
}
