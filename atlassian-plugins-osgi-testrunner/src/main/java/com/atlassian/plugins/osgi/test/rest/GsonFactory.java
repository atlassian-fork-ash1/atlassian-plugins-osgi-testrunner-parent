package com.atlassian.plugins.osgi.test.rest;

import com.atlassian.upm.api.util.Option;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class GsonFactory {

    private static GsonBuilder instance = null;

    protected GsonFactory() {}

    public static Gson getGson() {
        return getInstance().create();
    }

    private static GsonBuilder getInstance() {
        if (instance == null) {
            instance = new GsonBuilder();
            instance.registerTypeAdapter(Annotation.class, new AnnotationTypeAdapter());
            instance.registerTypeAdapter(Option.class, new UpmOptionAdapter());
            instance.registerTypeAdapter(AtomicInteger.class, new AtomicIntegerTypeAdapter());
            instance.registerTypeAdapter(AtomicLong.class, new AtomicLongTypeAdapter());
            instance.registerTypeAdapterFactory(new ClassTypeAdapterFactory());
            instance.addDeserializationExclusionStrategy(new ExclusionStrategy() {
                @Override
                public boolean shouldSkipClass(Class<?> clazz) {
                    return clazz.equals(Serializable.class);
                }

                @Override
                public boolean shouldSkipField(FieldAttributes f) {
                    return false;
                }
            });
        }
        return instance;
    }
}
