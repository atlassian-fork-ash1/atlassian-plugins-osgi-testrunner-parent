package com.atlassian.plugins.osgi.test.rest;

import com.atlassian.plugin.util.ClassLoaderStack;
import com.atlassian.plugins.osgi.test.OsgiTestClassLoader;
import com.atlassian.plugins.osgi.test.util.Pair;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRule;

import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class TestRunnerResourceTest {
    @Rule
    public MockitoJUnitRule mockitoRule = new MockitoJUnitRule(this);

    @Mock
    OsgiTestClassLoader testClassLoader;

    TestRunnerResource runner;

    static final ClassLoader classLoader = TestRunnerResourceTest.class.getClassLoader();


    @Before
    public void setUp() throws Exception {
        runner = new TestRunnerResource(testClassLoader);
    }

    public static class ExternalTestClass {
        @Test
        public void testContextClassLoader() throws Exception {
            assertThat("Test classes should be executed with the thread context class loader for the test class",
                    Thread.currentThread().getContextClassLoader(), sameInstance(classLoader));
        }
    }

    @Test
    public void executeTestShouldSetContextClassLoader() throws Exception {
        // having
        final String testClassName = "executeTest_shouldSetContextClassLoader";
        ClassLoaderStack.push(new ClassLoader() {});
        when(testClassLoader.findTestInstance(testClassName)).thenReturn(Pair.<String, Object>pair("bundle", new ExternalTestClass()));

        // when
        final TestRunnerResource.BundleResultAndTestClass result = runner.executeTest(testClassName);

        // then
        assertThat(result.getResult().getPassedMethods(), contains("testContextClassLoader"));
    }

    @After
    public void tearDown() throws Exception {
        while (ClassLoaderStack.pop() != null) {
            // ignored
        }
    }
}