package com.atlassian.plugins.osgi.test.asm;

import com.atlassian.plugins.osgi.test.util.Pair;
import org.apache.commons.io.IOUtils;
import org.objectweb.asm.ClassReader;
import org.osgi.framework.Bundle;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

/**
 * @since version
 */
public class BundleTestScanner
{
    public Pair<Set<Class<?>>, Set<Class<?>>> scan(Bundle bundle)
    {
        final Set<Class<?>> unitTests = new HashSet<Class<?>>();
        final Set<Class<?>> itTests = new HashSet<Class<?>>();

        final Enumeration<URL> urls = bundle.findEntries("/","*.class",true);
        if (urls != null)
        {
            final URL[] urlArray = Collections.<URL>list(urls).toArray(new URL[0]);

            for (URL url : urlArray)
            {
                processUrl(url, bundle, urlArray, unitTests, itTests);
            }
        }
        
        return Pair.pair(unitTests,itTests);
        
    }

    private void processUrl(URL url, Bundle bundle, URL[] urls, Set<Class<?>> unitTests, Set<Class<?>> itTests)
    {
        InputStream is = null;
        try
        {
            is = url.openStream();
            ClassReader classReader = new ClassReader(is);
            classReader.accept(new BundleClassVisitor(bundle,url,urls,unitTests,itTests), ClassReader.SKIP_CODE | ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES);
        }
        catch (IOException e)
        {
            //screw it
        }
        finally {
            IOUtils.closeQuietly(is);
        }

    }
}
