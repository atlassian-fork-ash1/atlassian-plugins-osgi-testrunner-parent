package it.com.example;


import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(AtlassianPluginsTestRunner.class)
public class MyPluginTest
{
    private final PluginAccessor pluginAccessor;

    public MyPluginTest(PluginAccessor pluginAccessor)
    {
        this.pluginAccessor = pluginAccessor;
    }

    @Test
    public void testSomething()
    {
        System.out.println("I RAN!!!!!!!!!!!!!");
        assert true;
    }

    @Test
    public void testSomeFailure()
    {
        System.out.println("I RAN But failed...");
        assertEquals("something failed","blah","boo");
    }

    @Test
    public void testPluginAccessor()
    {
        assertNotNull("plugin accessor is null",pluginAccessor);
        System.out.println("pluginAccessor = " + pluginAccessor);
    }

    @Ignore
    @Test
    public void testIgnoreMe()
    {
        assertNotNull("plugin accessor is null",pluginAccessor);
        System.out.println("pluginAccessor = " + pluginAccessor);
    }
}
